resource "azurerm_policy_definition" "policy" {
  name         = "subnet-attached-NSG"
  policy_type  = "Custom"
  mode         = "All"
  display_name = "subnet-attached-NSG"

  metadata = <<METADATA
    {
    "category": "General"
    }

METADATA


  policy_rule = <<POLICY_RULE
    {
    "if": {
        "anyOf": [
          {
            "allOf": [
              {
                "field": "type",
                "equals": "Microsoft.Network/virtualNetworks"
              },
              {
                "field": "Microsoft.Network/virtualNetworks/subnets[*].networkSecurityGroup.id",
                "exists": "false"
              }
            ]
          },
          {
            "allOf": [
              {
                "field": "type",
                "equals": "Microsoft.Network/virtualNetworks/subnets"
              },
              {
                "field": "Microsoft.Network/virtualNetworks/subnets/networkSecurityGroup.id",
                "exists": "false"
              }
            ]
          }
        ]
      },
      "then": {
        "effect": "deny"
      }
    }
    

POLICY_RULE

}

data "azurerm_subscription" "current" {
}

resource "azurerm_policy_assignment" "example" {
  name                 = "subnet-with-NSG"
  scope                = data.azurerm_subscription.current.id
  policy_definition_id = azurerm_policy_definition.policy.id
  description          = "All subnet should be attached with NSG"
  display_name         = "subnet-with-NSG"

}

resource "azurerm_resource_group" "rg" {
  name     = "testing-rg"
  location = "East US"
}

resource "azurerm_virtual_network" "hub-vnet" {
  name                = "testing-vnet"
  resource_group_name = azurerm_resource_group.rg.name
  location            = "East US"
  address_space       = ["10.111.0.0/20"]
}


resource "azurerm_subnet" "subnet" {
  name                 = "testing-vnet"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.hub-vnet.name
  address_prefix       = "10.111.0.0/23"
}
